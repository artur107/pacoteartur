<?php

require_once 'vendor/autoload.php';

use arturalves\pacoteartur\Exemplo;

describe( "Exemplo", function(){

    describe( "nome", function(){

        it( "contém 'pacote'", function(){
            $exemplo = new Exemplo();
            expect( $exemplo->nome() )->toContain( 'Pacote' );
        });

    });
});


?>