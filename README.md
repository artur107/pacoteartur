# Pacote Artur

> Pacote de exemplo

## Instalação

> Requer PHP7 e a extensão `mbstring`

```bash
composer require arturalves/pacoteartur
```

## API

```
namespace arturalves/pacoteartur;
class Exemplo {
    function nome();
}
```

## Licença

MIT